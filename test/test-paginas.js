var expect = require('chai').expect; // para gestionar variables
var $ = require('chai-jquery');
var request = require('request'); // para realizar peticiones

// it es la palabra clave del framework chai para ejecutar tests

// con describe se crea una suit de pruebas
describe("Pruebas sencillas", function() {
  // Con esta prueba se revisa si Node.js está bien instalado
  it('Test suma', function() {
    expect(9+4).to.equal(13);
    //console.log("Test suma: Prueba completada");
  });
});

describe("Pruebas de red", function() {
  // Verificar conexión a internet
  // done es una función a modo de callback
  it('Test internet', function(done) {
    request.get("http://www.diariosur.es",
      function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done(); // para resolver la respuesta asíncrona de la url
    });
  });

  // Verificar que aplicación escucha por el puerto 8081 (si lanzas con npm start)
  it('Test local', function(done) {
    request.get("http://localhost:8081",
        function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          done(); // para resolver la respuesta asíncrona de la url
    });
  });

  // Verificar que el body contiene un H1 con "Bienvenido..."
  it('Test body', function(done) {
    request.get("http://localhost:8081",
        function(error, response, body) {
          expect(body).contains("<h1>Bienvenido a mi blog</h1>");
          //expect(body).should.exist('<h1>\w*</h1>');
          done();
      });
  });
});

// PENDIENTE DE QUE ANGEL LO CORRIJA
describe("Test contenido HTML", function() {
  it('Test H1', function() {
    request.get("http://localhost:8081",
        function(error, response, body) {
          expect($('body h1')).to.have.text("Bienvenido a mi blog");
          done();
      });
  });
});
