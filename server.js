/* jshint -W097 */
/* jshint -W117 */
/* jshint esversion:6 */

'use strict'; // para obligar a declarar variables

const express = require('express'); // la variable express referencia al paquete express
const path = require('path'); // necesario para resolver rutas

// Constantes
const PORT = 8081;

// App
const app = express(); // para inicializar el servidor

app.use(express.static(__dirname)); // para que el html localice los js

// Cuando se navegue al localhost:8080 se ejecuta la función que se define aquí
app.get('/', function(req, res) {
  //res.send("Bienvenido al servidor web del Blog de Carlos\n");
  // __dirname es una variable de Node.js que contiene la raiz
  res.sendFile(path.join(__dirname + '/index.html'));
});

// Cuando se pida leer un post, se accede a esta petición
app.get('/detallePost/:id', function(req, res) {
  res.sendFile(path.join(__dirname + '/detallePost.html'));
});

app.get('/admin/gauge', function(req, res) {
  res.sendFile(path.join(__dirname + '/gauge.html'));
});

app.listen(PORT);
console.log('Express running at port ' + PORT);
