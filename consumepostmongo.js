/* jshint -W104 */
/* jshint -W069 */

// Mi BD Mongo es dbbootcamp
// Api Key de dbbootcamp: w07oucS_CXo8lV9vJ3PwVUncnAPP_B4T
const URL = "https://api.mlab.com/api/1/databases/dbbootcamp/collections/posts?apiKey=w07oucS_CXo8lV9vJ3PwVUncnAPP_B4T";
var response;

function obtenerPosts() {
  var peticion = new XMLHttpRequest();
  // Estas peticiones son asíncronas
  peticion.open("GET", URL, false); // false: síncrono
  // Por defecto la respuesta es en formato xml. Lo pedimos en formato json
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  // Como hemos hecho la petición síncrona, la respuesta deberíamos tenerla a continuación
  response = JSON.parse(peticion.responseText);
  // Lo guardo para cuando necesite consultar el detalle de un post
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  //mostrarPosts(); // No invocar si no está la tabla en index.html
}

// Esta función se utiliza para rellenar la tabla con los post
function mostrarPosts() {
  var tabla = document.getElementById("tablaPosts");
  for (var i = 0; i < response.length; i++) {
    //alert(response[i].titulo);
    var fila = tabla.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaAutor = fila.insertCell(3);
    var celdaAcciones = fila.insertCell(4);

    celdaId.innerHTML = response[i]._id.$oid;
    celdaTitulo.innerHTML = response[i].titulo;
    celdaTexto.innerHTML = response[i].texto;
    if (response[i].autor != undefined)
      celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
    else
      celdaAutor.innerHTML = "Anónimo";

    celdaAcciones.innerHTML = '<button class="btn btn-primary" onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>';
    celdaAcciones.innerHTML += ' <button class="btn btn-primary" onclick=\'borrarPost("' + celdaId.innerHTML + '")\';>Borrar</button>';
  }
}

function anadirPost() {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false); // false: síncrono
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Nuevo POST desde Atom", "texto":"Nuevo texto desde Atom", "autor":{"nombre":"Atom", "apellido":"Project"}}');
}

function actualizarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem =  "https://api.mlab.com/api/1/databases/dbbootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=w07oucS_CXo8lV9vJ3PwVUncnAPP_B4T";
  peticion.open("PUT", URLItem, false); // false: síncrono
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Nuevo POST desde Atom *updated*"}');
  peticion.send('{"titulo":"Nuevo POST desde Atom *updated*", "texto":"Nuevo texto desde Atom", "autor":{"nombre":"Atom", "apellido":"Project"}}');
}

function borrarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem =  "https://api.mlab.com/api/1/databases/dbbootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=w07oucS_CXo8lV9vJ3PwVUncnAPP_B4T";
  peticion.open("DELETE", URLItem, false); // false: síncrono
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
}

function seleccionarPost(numero) {
  sessionStorage["seleccionado"] = numero;
}

// Para rellenar campos en detallePost.html
function buscarDetallesPost(numero) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i=0; i < posts.length; i++) {
    if (posts[i]._id.$oid == numero) {
      // Mostrar detalles
      document.getElementById("h1").innerHTML = numero;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].texto;
      break;
    }
  }
}
